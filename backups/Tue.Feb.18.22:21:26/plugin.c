/*
 * SK64 F*ck You Plugin
 *
 * Copyright (c) 2013-2014 SpecterNetworks Development
 */

/*
 * Note to shock:
 * please structure braces like so:
 * 
 * if (herp)
 * {
 * 		derp;
 * }
 * 
 * instead of 
 * 
 * if (herp) {
 * 		derp;
 * }
 * 
 * it makes code more readable
 * ty :)
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "public_errors.h"
#include "public_errors_rare.h"
#include "public_definitions.h"
#include "public_rare_definitions.h"
#include "ts3_functions.h"
#include "plugin.h"

int kickdisabled=0;
int messageonly=0;

static struct TS3Functions ts3Functions;

#ifdef _WIN32 // DO NOT REMOVE
#define _strcpy(dest, destSize, src) strcpy_s(dest, destSize, src)
#define snprintf sprintf_s
#else
#define _strcpy(dest, destSize, src) { strncpy(dest, src, destSize-1); (dest)[destSize-1] = '\0'; }
#endif

void ts3plugin_freeMemory(void* data) {
	free(data);
}

#define PLUGIN_API_VERSION 19

#define PATH_BUFSIZE 512
#define COMMAND_BUFSIZE 128
#define INFODATA_BUFSIZE 128
#define SERVERINFO_BUFSIZE 256
#define CHANNELINFO_BUFSIZE 512
#define RETURNCODE_BUFSIZE 128

static char* pluginID = NULL;


/**********************************0x40's hax. ^This way up*********************************/
static struct PluginMenuItem* createMenuItem(enum PluginMenuType type, int id, const char* text, const char* icon) {
	struct PluginMenuItem* menuItem = (struct PluginMenuItem*)malloc(sizeof(struct PluginMenuItem));
	menuItem->type = type;
	menuItem->id = id;
	_strcpy(menuItem->text, PLUGIN_MENU_BUFSZ, text);
	_strcpy(menuItem->icon, PLUGIN_MENU_BUFSZ, icon);
	return menuItem;
}
void ts3plugin_registerPluginID(const char* id) {
	const size_t sz = strlen(id) + 1;
	pluginID = (char*)malloc(sz * sizeof(char));
	_strcpy(pluginID, sz, id);  /* The id buffer will invalidate after exiting this function */
	printf("PLUGIN: registerPluginID: %s\n", pluginID);
}
/*********************************** Required functions ************************************/
/*
 * If any of these required functions is not implemented, TS3 will refuse to load the plugin
 */

/* Unique name identifying this plugin */
const char* ts3plugin_name() 
{
	//return "F*ck You";
	return "SK64";
}

/* Plugin version */
const char* ts3plugin_version() 
{
    return "2.3";
}

/* Plugin API version. Must be the same as the clients API major version, else the plugin fails to load. */
int ts3plugin_apiVersion() 
{
	return PLUGIN_API_VERSION;
}

/* Plugin author */
const char* ts3plugin_author() 
{
	/* If you want to use wchar_t, see ts3plugin_name() on how to use */
    return "Segfault64 and Shock (SK64)";
}

/* Plugin description */
const char* ts3plugin_description() 
{
	/* If you want to use wchar_t, see ts3plugin_name() on how to use */
    return "This plugin fucks around with noobs. Great for trolling and showing off. Coded by Segfault64 and shock.";
}

/* Set TeamSpeak 3 callback functions */
void ts3plugin_setFunctionPointers(const struct TS3Functions funcs) 
{
    ts3Functions = funcs;
}

/*
 * Custom code called right after loading the plugin. Returns 0 on success, 1 on failure.
 * If the function returns 1 on failure, the plugin will be unloaded again.
 */
int ts3plugin_init() 
{
    char appPath[PATH_BUFSIZE];
    char resourcesPath[PATH_BUFSIZE];
    char configPath[PATH_BUFSIZE];
	char pluginPath[PATH_BUFSIZE];

    /* Your plugin init code here */
    printf("[OK] SK64 init\n");

    /* Example on how to query application, resources and configuration paths from client */
    /* Note: Console client returns empty string for app and resources path */
    ts3Functions.getAppPath(appPath, PATH_BUFSIZE);
    ts3Functions.getResourcesPath(resourcesPath, PATH_BUFSIZE);
    ts3Functions.getConfigPath(configPath, PATH_BUFSIZE);
	ts3Functions.getPluginPath(pluginPath, PATH_BUFSIZE);

	printf("PLUGIN: App path: %s\nResources path: %s\nConfig path: %s\nPlugin path: %s\n", appPath, resourcesPath, configPath, pluginPath);

    return 0;  /* 0 = success, 1 = failure, -2 = failure but client will not show a "failed to load" warning */
	/* -2 is a very special case and should only be used if a plugin displays a dialog (e.g. overlay) asking the user to disable
	 * the plugin again, avoiding the show another dialog by the client telling the user the plugin failed to load.
	 * For normal case, if a plugin really failed to load because of an error, the correct return value is 1. */
}

/* Custom code called right before the plugin is unloaded */
void ts3plugin_shutdown() 
{
    /* Your plugin cleanup code here */
    printf("SK64 shutdown\n");

	/*
	 * Note:
	 * If your plugin implements a settings dialog, it must be closed and deleted here, else the
	 * TeamSpeak client will most likely crash (DLL removed but dialog from DLL code still open).
	 */

	/* Free pluginID if we registered it */
	if(pluginID) 
	{
		free(pluginID);
		pluginID = NULL;
	}
}
	

/* Plugin */

/* Is this needed? 
 * // 18/2/2014 0x40: nope, it is just a forward declaration
void ts3plugin_onClientBanFromServerEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID kickerID, const char* kickerName, const char* kickerUniqueIdentifier, uint64 time, const char* kickMessage) {
}
*/

int ts3plugin_onClientPokeEvent(uint64 serverConnectionHandlerID, anyID fromClientID, const char* pokerName, const char* pokerUniqueIdentity, const char* message, int ffIgnored) 
{
    anyID myID;

    printf("PLUGIN onClientPokeEvent: %llu %d %s %s %d\n", (long long unsigned int)serverConnectionHandlerID, fromClientID, pokerName, message, ffIgnored);

/* We dont want ff, if we have blocked someone we still want to kick them, maybe have this as a setting?

	Check if the Friend/Foe manager has already blocked this poke
	if(ffIgnored) {
		return 0;  Client will block anyways, doesn't matter what we return
	}
*/

    /* Our if statements, also getting our own client ID. */
    if(ts3Functions.getClientID(serverConnectionHandlerID, &myID) != ERROR_ok) /* Get own client ID */
    {  
        ts3Functions.logMessage("Error querying own client id", LogLevel_ERROR, "Plugin", serverConnectionHandlerID);
        return 0;
    }
    
    if(fromClientID != myID && messageonly==0 && kickdisabled==1) {  /* Don't reply when source is own client, might cause a major fuck about on the future kick detection. */

		/* Send a message to the client who kicked you, make enable/disable a setting, and the message a setting. Errormessage can stay. */
        if (kickdisabled==0 || messageonly==1)
        {
	        if(ts3Functions.requestSendPrivateTextMsg(serverConnectionHandlerID, "Poke Ignored! Send it as a PM you lazy bugger! (SK64 F*ck You plugin)", fromClientID, NULL) != ERROR_ok) 
	        {
	            ts3Functions.logMessage("Error pm'ing user who just poked you - do you have enough permissions?", LogLevel_ERROR, "Plugin", serverConnectionHandlerID);
	        }
		}
		
		/* Kick Client, Make a setting also, error message stays, but kick message must be a setting. */
        if (kickdisabled==0 && messageonly!=1)
        {
			if(ts3Functions.requestClientKickFromServer(serverConnectionHandlerID, fromClientID, "Don't f*cking poke me! (SK64 F*ck You plugin)", NULL) != ERROR_ok) 
	        {
	            ts3Functions.logMessage("Error kicking user who just poked you - do you have enough permissions?", LogLevel_ERROR, "Plugin", serverConnectionHandlerID);
	        }
		}
		return 1;
    } else {
		return 0;
	}
	
    return 1;  /* Ignore the poke. */
}

/*------------------------------------------------------------------------------------*/
/*--------------------------MENU IS HERE DO NOT BREAK---------------------------------*/
/*------------------------------------------------------------------------------------*/
enum 
{
	DISABLE_KICK,
	ENABLE_KICK,
	MESSAGE_ONLY
};


/* Some macros to make the code to create menu items a bit more readable */
#define BEGIN_CREATE_MENUS(x) const size_t sz = x + 1; size_t n = 0; *menuItems = (struct PluginMenuItem**)malloc(sizeof(struct PluginMenuItem*) * sz);
#define CREATE_MENU_ITEM(a, b, c, d) (*menuItems)[n++] = createMenuItem(a, b, c, d);
#define END_CREATE_MENUS (*menuItems)[n++] = NULL; assert(n == sz);

void ts3plugin_initMenus(struct PluginMenuItem*** menuItems, char** menuIcon)
{
	BEGIN_CREATE_MENUS(3);  /* IMPORTANT: Number of menu items must be correct! */
	
	CREATE_MENU_ITEM(PLUGIN_MENU_TYPE_GLOBAL,  ENABLE_KICK,  "Enable Kick", "");
	CREATE_MENU_ITEM(PLUGIN_MENU_TYPE_GLOBAL,  MESSAGE_ONLY,  "Message Only", "");
	CREATE_MENU_ITEM(PLUGIN_MENU_TYPE_GLOBAL,  DISABLE_KICK,  "Disabled", "");
	END_CREATE_MENUS;  /* Includes an assert checking if the number of menu items matched */
	
	*menuIcon = (char*)malloc(PLUGIN_MENU_BUFSZ * sizeof(char));
	_strcpy(*menuIcon, PLUGIN_MENU_BUFSZ, "t.png");
	
	ts3Functions.setPluginMenuEnabled(pluginID, ENABLE_KICK, 0);
}

void ts3plugin_onMenuItemEvent(uint64 serverConnectionHandlerID, enum PluginMenuType type, int menuItemID, uint64 selectedItemID)
{
	switch(menuItemID)
	{
		case ENABLE_KICK:
			ts3Functions.setPluginMenuEnabled(pluginID, ENABLE_KICK, 0);
			ts3Functions.setPluginMenuEnabled(pluginID, MESSAGE_ONLY, 1);
			ts3Functions.setPluginMenuEnabled(pluginID, DISABLE_KICK, 1);
			kickdisabled=0;
			messageonly=0;
			break;
		case DISABLE_KICK:
			ts3Functions.setPluginMenuEnabled(pluginID, ENABLE_KICK, 1);
			ts3Functions.setPluginMenuEnabled(pluginID, DISABLE_KICK, 0);
			ts3Functions.setPluginMenuEnabled(pluginID, MESSAGE_ONLY, 1);
			kickdisabled=1;
			messageonly=0;
			break;
		case MESSAGE_ONLY:
			ts3Functions.setPluginMenuEnabled(pluginID, ENABLE_KICK, 1);
			ts3Functions.setPluginMenuEnabled(pluginID, DISABLE_KICK, 1);
			ts3Functions.setPluginMenuEnabled(pluginID, MESSAGE_ONLY, 0);
			kickdisabled=1;
			messageonly=1;
			
			break;
		default:
			break;
	}
}
