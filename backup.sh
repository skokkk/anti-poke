#!/bin/bash
dir="`date|awk '{print $1"." $2"." $3"." $4}'`"
if [ ! -d backups ]
then
	 mkdir backups
fi

mkdir backups/"$dir"
cp src/*.c backups/"$dir"
cp src/*.h backups/"$dir"
cp src/*.sh backups/"$dir"
